package com.cafetiere.testimonial.controller;

import com.cafetiere.testimonial.model.Testimonials;
import com.cafetiere.testimonial.service.TestimonialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(path = "/testimonials")
public class TestimonialsController {
    @Autowired
    public TestimonialsService testimonialsService;

    @PostMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Testimonials> createTestimonials(
            @RequestBody Testimonials testimonials,
            @PathVariable(value = "id") int id) {
        return ResponseEntity.ok(testimonialsService.createTestimonials(testimonials, id));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Testimonials> getTestimonials(
            @PathVariable(value = "id") int id) {
        return ResponseEntity.ok(testimonialsService.getTestimonialsById(id));
    }

    @GetMapping(path = "/list-testimonials/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<List<Testimonials>> getListTestimonials(
            @PathVariable(value = "id") int id) {
        return ResponseEntity.ok(testimonialsService.getListTestimonialsById(id));
    }
}
