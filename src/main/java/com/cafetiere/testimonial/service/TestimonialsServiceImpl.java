package com.cafetiere.testimonial.service;

import com.cafetiere.testimonial.model.Testimonials;
import com.cafetiere.testimonial.repository.TestimonialsRepository;
import com.cafetiere.testimonial.vo.Account;
import com.cafetiere.testimonial.vo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestimonialsServiceImpl implements TestimonialsService {
    @Autowired
    private TestimonialsRepository testimonialsRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Testimonials createTestimonials(Testimonials testimonials, int id) {
        testimonials.setProductId(id);
        testimonials.setAccountId(id);
        testimonialsRepository.save(testimonials);
        return testimonials;
    }

    @Override
    public Testimonials getTestimonialsById(int id) {
        return testimonialsRepository.findById(id);
    }

    @Override
    public List<Testimonials> getListTestimonialsById(int id) {
        Product product = restTemplate.getForObject("http://PRODUCT-SERVICE/product/"
                        + id, Product.class);
        Account account = restTemplate.getForObject("http://AUTH-SERVICE/user/"
                + id, Account.class);
        List<Testimonials> allList = testimonialsRepository.findAll();
        List<Testimonials> list = new ArrayList<Testimonials>();
        for (Testimonials testi : allList) {
            if (testi.getProductId() == id && product.getId() == id && account.getId() == id) {
                list.add(testi);
            }
        }
        return list;

    }
}
