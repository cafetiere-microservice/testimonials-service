package com.cafetiere.testimonial.service;

import com.cafetiere.testimonial.model.Testimonials;

import java.util.List;

public interface TestimonialsService {
    Testimonials createTestimonials(Testimonials testimonials, int id);

    Testimonials getTestimonialsById(int id);

    List<Testimonials> getListTestimonialsById(int id);
}
