package com.cafetiere.testimonial.repository;

import com.cafetiere.testimonial.model.Testimonials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestimonialsRepository extends JpaRepository<Testimonials, Integer> {
    Testimonials findById(int id);
}
