package com.cafetiere.testimonial.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "testimonials")
@Data
@NoArgsConstructor
public class Testimonials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "harga", nullable = false)
    private int harga;

    @Column(name = "rasa", nullable = false)
    private String rasa;

    @Column(name = "pelayanan", nullable = false)
    private String pelayanan;

    @Column(name = "product_id", nullable = false)
    private int productId;

    @Column(name = "account_id", nullable = false)
    private int accountId;

    /**
     * Constructor testimonials.
     */

    public Testimonials(int harga, String rasa, String pelayanan) {
        this.harga = harga;
        this.rasa = rasa;
        this.pelayanan = pelayanan;
    }
}
