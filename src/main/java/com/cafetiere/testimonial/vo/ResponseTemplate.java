package com.cafetiere.testimonial.vo;

import com.cafetiere.testimonial.model.Testimonials;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplate {
    private Testimonials testimonials;
    private Product product;
    private Account account;
}
