package com.cafetiere.testimonial.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
}
