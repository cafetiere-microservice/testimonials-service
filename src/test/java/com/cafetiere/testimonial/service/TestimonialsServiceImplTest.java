package com.cafetiere.testimonial.service;

import com.cafetiere.testimonial.model.Testimonials;
import com.cafetiere.testimonial.repository.TestimonialsRepository;
import com.cafetiere.testimonial.service.TestimonialsService;
import com.cafetiere.testimonial.vo.Account;
import com.cafetiere.testimonial.vo.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class TestimonialsServiceImplTest {
    @Mock
    private RestTemplate restTemplate;

    @Mock
    private TestimonialsRepository testimonialsRepository;

    @InjectMocks
    private TestimonialsServiceImpl testimonialsService;

    private Account account;
    private Account account2;
    private Product product;
    private Product product2;
    private Testimonials testimonials;

    @BeforeEach
    void setUp() {
        product = new Product(1, "latte", "enak", 10000, 1);
        account = new Account(1, "hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu");
        product2 = new Product(2, "latte", "enak", 10000, 1);
        account2 = new Account(2, "hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu");
        testimonials = new Testimonials(5000, "enak", "bagus");
        testimonials.setId(1);
        testimonials.setProductId(product.getId());
        testimonials.setAccountId(account.getId());
    }

    @Test
    void testCreateTestimonials() {
        Testimonials dummy;
        lenient().when(testimonialsRepository.findById(product.getId()))
                .thenReturn(testimonials);
        dummy = testimonialsService.createTestimonials(testimonials, product.getId());
        assertNotEquals(null, dummy);
    }

    @Test
    void testGetTestimonialsById() {
        lenient().when(testimonialsRepository.findById(testimonials.getId()))
                .thenReturn(testimonials);
        Testimonials testimonialsResult = testimonialsService.getTestimonialsById(
                testimonials.getId());
        assertEquals(testimonials.getId(), testimonialsResult.getId());
    }

    @Test
    public void testGetListTestimonialsById() {
        List<Testimonials> listTestimonials = testimonialsRepository.findAll();
//        List<Testimonials> testimonials1 = new ArrayList<Testimonials>();
        lenient().when(testimonialsRepository.findAll())
                .thenReturn(listTestimonials);
        lenient().when(restTemplate.getForObject("http://PRODUCT-SERVICE/product/"
                + 1, Product.class))
                .thenReturn(product);
        lenient().when(restTemplate.getForObject("http://AUTH-SERVICE/user/"
                + 1, Account.class))
                .thenReturn(account);
        for (Testimonials testi : listTestimonials) {
            assertEquals(1, testi.getProductId());
            assertEquals(1, testi.getAccountId());
//            lenient().when(testimonialsRepository.findById(product.getId()))
//                    .thenReturn(testi);
//            lenient().when(testimonialsRepository.findById(account.getId()))
//                    .thenReturn(testi);
        }
        List<Testimonials> listTestimonialsResult = testimonialsService.getListTestimonialsById(
                1);
        assertEquals(listTestimonials, listTestimonialsResult);
    }

//    @Test
//    public void testGetListTestimonialsByIdNull() {
//        List<Testimonials> listTestimonials = testimonialsRepository.findAll();
//        lenient().when(testimonialsRepository.findAll())
//                .thenReturn(listTestimonials);
//        lenient().when(restTemplate.getForObject("http://PRODUCT-SERVICE/product/"
//                + 2, Product.class))
//                .thenReturn(product2);
//        lenient().when(restTemplate.getForObject("http://AUTH-SERVICE/user/"
//                + 2, Account.class))
//                .thenReturn(account2);
//        List<Testimonials> listTestimonialsResult = [];
//        assertEquals(listTestimonialsResult, testimonialsService.getListTestimonialsById(
//                2));
//    }

}
