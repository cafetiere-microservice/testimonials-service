package com.cafetiere.testimonial.controller;

import com.cafetiere.testimonial.model.Testimonials;
import com.cafetiere.testimonial.service.TestimonialsServiceImpl;
import com.cafetiere.testimonial.vo.Account;
import com.cafetiere.testimonial.vo.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TestimonialsController.class)
class TestimonialsControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TestimonialsServiceImpl testimonialsService;

    private Account account;
    private Product product;
    private Testimonials testimonials;

    @BeforeEach
    void setUp() {
        product = new Product(1, "latte", "enak", 10000, 1);
        account = new Account(1, "hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu");
        testimonials = new Testimonials(
                16000, "enak", "kurs");
        testimonials.setId(1);
        testimonials.setProductId(product.getId());
        testimonials.setAccountId(account.getId());
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerPostTestimonials() throws Exception {
        when(testimonialsService.createTestimonials(testimonials, 1))
                .thenReturn(testimonials);
        mvc.perform(post("/testimonials/1").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(testimonials))).andExpect(jsonPath("$.id").value(1));
    }

    @Test
    void testControllerGetTestiById() throws Exception {

        when(testimonialsService.getTestimonialsById(testimonials.getId()))
                .thenReturn(testimonials);
        mvc.perform(get("/testimonials/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(testimonials.getId()));
    }

    @Test
    void testControllerGetListTestiById() throws Exception {
        List testimonialsList = new ArrayList<Testimonials>();
        testimonialsList.add(testimonials);
        when(testimonialsService.getListTestimonialsById(product.getId()))
                .thenReturn(testimonialsList);
        mvc.perform(get("/testimonials/list-testimonials/1")).andExpect(status().isOk());
    }

}
