package com.cafetiere.testimonial.vo;

import com.cafetiere.testimonial.model.Testimonials;
import com.cafetiere.testimonial.vo.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {
    private Account account;

    @BeforeEach
    void setUp() {
        account = new Account(1, "hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu");
    }

    @Test
    void testSetAccountId() {
        account.setId(
                1);
        assertEquals(1,
                account.getId());
    }

    @Test
    void testGetAccountId() {
        assertEquals(1,
                account.getId());
    }

    @Test
    void testSetAccountFirstName() {
        account.setFirstName(
                "hasna");
        assertEquals("hasna",
                account.getFirstName());
    }

    @Test
    void testGetAccountFirstName() {
        assertEquals("hasna",
                account.getFirstName());
    }

    @Test
    void testSetAccountLastName() {
        account.setLastName(
                "nadifah");
        assertEquals("nadifah",
                account.getLastName());
    }

    @Test
    void testGetAccountLastName() {
        assertEquals("nadifah",
                account.getLastName());
    }

    @Test
    void testSetAccountEmail() {
        account.setEmail(
                "haha@gmail.com");
        assertEquals("haha@gmail.com",
                account.getEmail());
    }

    @Test
    void testGetAccountEmail() {
        assertEquals("haha@gmail.com",
                account.getEmail());
    }

    @Test
    void testSetAccountUsername() {
        account.setUsername(
                "hnadifahsn");
        assertEquals("hnadifahsn",
                account.getUsername());
    }

    @Test
    void testGetAccountUsername() {
        assertEquals("hnadifahsn",
                account.getUsername());
    }

    @Test
    void testSetAccountPassword() {
        account.setPassword(
                "huu");
        assertEquals("huu",
                account.getPassword());
    }

    @Test
    void testGetAccountPassword() {
        assertEquals("huu",
                account.getPassword());
    }
}
