package com.cafetiere.testimonial.vo;

import com.cafetiere.testimonial.vo.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {
    private Product product;

    @BeforeEach
    void setUp() {
        product = new Product(1, "latte", "enak", 10000, 1);
    }

    @Test
    void testSetProductId() {
        product.setId(
                1);
        assertEquals(1,
                product.getId());
    }

    @Test
    void testGetProductId() {
        assertEquals(1,
                product.getId());
    }

    @Test
    void testSetProductName() {
        product.setName(
                "latte");
        assertEquals("latte",
                product.getName());
    }

    @Test
    void testGetProductName() {
        assertEquals("latte",
                product.getName());
    }

    @Test
    void testSetProductDescription() {
        product.setDescription(
            "enak");
        assertEquals("enak",
                product.getDescription());
    }

    @Test
    void testGetProductDescription() {
        assertEquals("enak",
                product.getDescription());
    }

    @Test
    void testSetProductPrice() {
        product.setPrice(
                10000);
        assertEquals(10000,
                product.getPrice());
    }

    @Test
    void testGetProductPrice() {
        assertEquals(10000,
                product.getPrice());
    }

    @Test
    void testSetProductQuantity() {
        product.setQuantity(
                1);
        assertEquals(1,
                product.getQuantity());
    }

    @Test
    void testGetProductQuantity() {
        assertEquals(1,
                product.getQuantity());
    }
}
