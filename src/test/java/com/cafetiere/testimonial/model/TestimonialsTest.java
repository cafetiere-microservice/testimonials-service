package com.cafetiere.testimonial.model;

import com.cafetiere.testimonial.vo.Account;
import com.cafetiere.testimonial.vo.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestimonialsTest {
    private Account account;
    private Product product;
    private Testimonials testimonials;

    @BeforeEach
    void setUp() {
        product = new Product(1, "latte", "enak", 10000, 1);
        account = new Account(1, "hasna", "nadifah", "haha@gmail.com", "hnadifahsn", "huu");
        testimonials = new Testimonials(
                16000, "enak", "kurs");
        testimonials.setId(1);
        testimonials.setProductId(product.getId());
        testimonials.setAccountId(account.getId());
    }


    @Test
    void testSetAccount() {
        testimonials.setAccountId(
                1);
        assertEquals(1,
                testimonials.getAccountId());
    }

    @Test
    void testGetAccount() {
        assertEquals(1,
                testimonials.getAccountId());
    }

    @Test
    void testSetProduct() {
        testimonials.setProductId(1);
    }

    @Test
    void testGetProduct() {
        assertEquals(1, testimonials.getProductId());
    }

    @Test
    void testSetHarga() {
        testimonials.setHarga(16000);
        assertEquals(16000, testimonials.getHarga());
    }

    @Test
    void testGetHarga() {
        assertEquals(16000, testimonials.getHarga());
    }

    @Test
    void testSetRasa() {
        testimonials.setRasa("enak");
        assertEquals("enak", testimonials.getRasa());
    }

    @Test
    void testGetRasa() {
        assertEquals("enak", testimonials.getRasa());
    }

    @Test
    void testSetPelayanan() {
        testimonials.setPelayanan("kurs");
        assertEquals("kurs", testimonials.getPelayanan());
    }

    @Test
    void testGetPelayanan() {
        assertEquals("kurs", testimonials.getPelayanan());
    }

}
